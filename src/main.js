import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
Vue.use(VueResource)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
new Vue({
  el: '#app',
  render: h => h(App)
})
